package com.adrien.fenech.calculatricetva;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubView;


public class MainActivity extends ActionBarActivity implements MoPubView.BannerAdListener, MoPubInterstitial.InterstitialAdListener {

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!block) {
                float current = Float.valueOf("0" + s);
                computeAll(current);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void reset() {
        block = true;
        b_ht = false;
        b_20 = false;
        b_10 = false;
        b_5_5 = false;
        b_8_5 = false;
        b_19_6 = false;
        b_7 = false;
        b_tva = false;
        b_custom = false;
        ((EditText)findViewById(R.id.ht)).setText("");
        ((EditText)findViewById(R.id.p20)).setText("");
        ((EditText)findViewById(R.id.p10)).setText("");
        ((EditText)findViewById(R.id.p5_5)).setText("");
        ((EditText)findViewById(R.id.p8_5)).setText("");
        ((EditText)findViewById(R.id.p19_6)).setText("");
        ((EditText)findViewById(R.id.p7)).setText("");
        ((EditText)findViewById(R.id.custom)).setText("");
        ((EditText)findViewById(R.id.ht)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p20)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p10)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p5_5)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p8_5)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p19_6)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.p7)).setTextColor(Color.rgb(200, 200, 200));
        ((EditText)findViewById(R.id.custom)).setTextColor(Color.rgb(200, 200, 200));
        ht = 0;
        percent = 0;
        isTTC = true;
        block = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        moPubView = (MoPubView) findViewById(R.id.adview);
        moPubView.setAdUnitId("6d4dc724ea254a189154d902f9d7b802");
        moPubView.loadAd();
        moPubView.setBannerAdListener(this);

        mInterstitial = new MoPubInterstitial(this, "e17ac01821ae4c08bea7631e8187247b");
        mInterstitial.setInterstitialAdListener(this);
        mInterstitial.load();

        ((EditText)findViewById(R.id.ht)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = false;
                percent = 0;
                b_ht = true;
                ((EditText) findViewById(R.id.ht)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.ht)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p20)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = 20;
                b_20 = true;
                ((EditText) findViewById(R.id.p20)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p20)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p10)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = 10;
                b_10 = true;
                ((EditText) findViewById(R.id.p10)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p10)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p5_5)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = (float) 5.5;
                b_5_5 = true;
                ((EditText) findViewById(R.id.p5_5)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p5_5)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p8_5)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = (float) 8.5;
                b_8_5 = true;
                ((EditText) findViewById(R.id.p8_5)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p8_5)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p19_6)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = (float) 19.6;
                b_19_6 = true;
                ((EditText) findViewById(R.id.p19_6)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p19_6)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.p7)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = 7;
                b_7 = true;
                ((EditText) findViewById(R.id.p7)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.p7)).addTextChangedListener(watcher);


        ((EditText)findViewById(R.id.custom)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                isTTC = true;
                percent = Float.valueOf(((EditText) findViewById(R.id.pcustom)).getText().toString());
                b_custom = true;
                ((EditText) findViewById(R.id.custom)).setTextColor(Color.rgb(200, 255, 200));
                return false;
            }
        });
        ((EditText)findViewById(R.id.custom)).addTextChangedListener(watcher);

        reset();
        ((Button)findViewById(R.id.calcul)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private float addTVA(float nb, float percent) {
        return (float)(Math.round((nb + (nb * (percent / 100))) * 1000.0) / 1000.0);
    }

    private float getFromPercent(float nb, float percent) {
        return nb / (1 + (percent / 100));
    }

    private void computeAll(float nb) {
        block = true;
        if (isTTC) {
            ht = getFromPercent(nb, percent);
            ((TextView)findViewById(R.id.ht)).setText(Float.toString(ht));
        }
        else
            ht = nb;

        if (!b_20)
            ((TextView)findViewById(R.id.p20)).setText(Float.toString(addTVA(ht, 20)));
        if (!b_10)
            ((TextView)findViewById(R.id.p10)).setText(Float.toString(addTVA(ht, 10)));
        if (!b_5_5)
            ((TextView)findViewById(R.id.p5_5)).setText(Float.toString(addTVA(ht, (float)5.5)));
        if (!b_8_5)
            ((TextView)findViewById(R.id.p8_5)).setText(Float.toString(addTVA(ht, (float)8.5)));
        if (!b_19_6)
            ((TextView)findViewById(R.id.p19_6)).setText(Float.toString(addTVA(ht, (float)19.6)));
        if (!b_7)
            ((TextView)findViewById(R.id.p7)).setText(Float.toString(addTVA(ht, 7)));
        if (!b_custom)
            ((TextView)findViewById(R.id.custom)).setText(Float.toString(addTVA(ht,
                    Float.valueOf("0" + ((TextView)findViewById(R.id.pcustom)).getText().toString()))));

        ((TextView) findViewById(R.id.tva)).setText(Float.toString(percent / 100 * nb));

        block = false;
    }

    // Declare an instance variable for your MoPubView.
    private MoPubView moPubView;
    private MoPubInterstitial mInterstitial;

    protected void onDestroy() {
        moPubView.destroy();
        mInterstitial.destroy();
        super.onDestroy();
    }

    float ht = 0;
    float tx = -1;

    float percent;
    boolean isTTC;
    boolean block = false;
    boolean b_ht, b_20, b_10, b_5_5, b_8_5, b_19_6, b_7, b_tva, b_custom;

    @Override
    public void onBannerLoaded(MoPubView banner) {

    }

    @Override
    public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {

    }

    @Override
    public void onBannerClicked(MoPubView banner) {

    }

    @Override
    public void onBannerExpanded(MoPubView banner) {

    }

    @Override
    public void onBannerCollapsed(MoPubView banner) {

    }

    @Override
    public void onInterstitialLoaded(MoPubInterstitial interstitial) {
        if (interstitial.isReady()) {
            mInterstitial.show();
        } else {
            // Other code
        }
    }

    @Override
    public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {

    }

    @Override
    public void onInterstitialShown(MoPubInterstitial interstitial) {

    }

    @Override
    public void onInterstitialClicked(MoPubInterstitial interstitial) {

    }

    @Override
    public void onInterstitialDismissed(MoPubInterstitial interstitial) {

    }
}
